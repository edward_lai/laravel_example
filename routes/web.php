<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('v1/user/create', 'MemberController@store');        //註冊
Route::post('v1/user/delete', 'MemberController@destroy');      //刪除
Route::post('v1/user/pwd/change', 'MemberController@update');   //編輯
Route::get('v1/user/login', 'LoginController@login');           //登入