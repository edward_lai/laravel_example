<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController
{
    public function sendResponse($code = 0, $result = [], $message = '', $httpCode = 200)
    {
        $response = [
            'Code'    => $code,
            'Message' => $message,
            'Result'  => ($result === []) ? null : $result,
        ];

        return response()->json($response, $httpCode);
    }
}