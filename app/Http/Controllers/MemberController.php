<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Support\Facades\Auth;
use Str;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\Hash;
use Exception;

class MemberController extends BaseController
{
    public function store(Request $request)
    {
        try {
            $request->validate([
                'account' => ['required', 'string', 'max:50'],
                'password' => ['required', 'string', 'min:6', 'max:50'],
            ]);
            $apiToken = Str::random(10);
            $create = Member::create([
                'account' => $request['account'],
                'password' => Hash::make($request['password']),
                'api_token' => $apiToken,
            ]);
            if ($create)
                return $this->sendResponse(0, ["IsOK" => true], '', 200);
            else
                return $this->sendResponse(2, ["IsOK" => false], 'create error', 404);
        } catch (Exception $e) {
            return $this->sendResponse(2, ["IsOK" => false], 'registered failed.', 500);
        }
    }

    public function update(Request $request)
    {
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                'account' => ['string', 'max:50'],
                'password' => ['string', 'min:6', 'max:50'],
            ]);
            if ($validator->fails()) {
                return $this->sendResponse(2, [], 'Validation Error', $validator->errors());
            }
            $member = Member::where('account', $request->account)->firstOrFail();
            $member->password = Hash::make($request->password);
            if ($member->update())
                return $this->sendResponse(0, ["IsOK" => true], '', 200);
            else
                return $this->sendResponse(2, ["IsOK" => false], 'create error', 404);
        } catch (Exception $e) {
            return $this->sendResponse(2, ["IsOK" => false], 'update failed.', 500);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $members = Member::where('account', $request->account)->firstOrFail();
            if ($members->delete())
                return $this->sendResponse(0, ["IsOK" => true], '', 200);
            else
                return $this->sendResponse(2, ["IsOK" => false], 'delete error', 400);
        } catch (Exception $e) {
            return $this->sendResponse(2, ["IsOK" => false], 'delete failed.', 500);
        }
    }
}