<?php

namespace App\Http\Controllers;

use Str;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Exception;

class LoginController extends BaseController
{
   public function login(Request $request)
   {
        $member = Member::where('account', $request->account)->firstOrFail();
        if(Hash::check($request->password, $member->password)){
            $apiToken = Str::random(10);
            if($member){
                return $this->sendResponse(0, [], '', 200);
            }
        }
        return $this->sendResponse(2, [], 'Login Failed', 400);
   }
}